import api from '@/services/api'

export default {
    fetchAttendences() {
        return api.get(`takeattd/`)
            .then(response => response.data)
    },
    postAttendence(payload) {
        return api.post(`takeattd/`, payload)
            .then(response => response.data)
    },
    deleteAttendence(id) {
        return api.delete(`takeattd/${id}`)
            .then(response => response.data)
    }
}