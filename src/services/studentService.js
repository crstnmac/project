import api from '@/services/api'

export default {
    fetchStudents() {
        return api.get(`students/`)
            .then(response => response.data)
    },
    postStudent(payload) {
        return api.post(`students/`, payload)
            .then(response => response.data)
    },
    deleteStudent(id) {
        return api.delete(`students/${id}`)
            .then(response => response.data)
    }
}