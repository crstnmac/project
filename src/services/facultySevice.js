import api from '@/services/api'

export default {
    fetchFaculties() {
        return api.get(`faculty/`)
            .then(response => response.data)
    },
    postFaculty(payload) {
        return api.post(`faculty/`, payload)
            .then(response => response.data)
    },
    deleteFaculty(id) {
        return api.delete(`faculty/${id}`)
            .then(response => response.data)
    }
}