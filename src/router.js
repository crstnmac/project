import Vue from "vue";
import Router from "vue-router";
import Home from "@/views/Home.vue";
import Login from "@/components/Login";
import TakeAttnd from "@/components/TakeAttnd";
Vue.use(Router);

//uncaught exception ignore -->

const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
	return originalPush.call(this, location).catch((err) => err);
};
// <--uncaught exception ignore

export default new Router({
	mode: "history",
	routes: [
		{
			path: "/",
			name: "Attendance",
			component: TakeAttnd,
		},
		{
			path: "/about",
			name: "About",
			component: Home,
		},

		{
			path: "/login",
			name: "Login",
			component: Login,
		},
	],
});
