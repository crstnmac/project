import facultyService from '../../services/facultySevice'

const state = {
    faculties: []
}

const getters = {
    faculties: state => {
        return state.faculties
    }
}

const actions = {
    getFaculties({
        commit
    }) {
        facultyService.fetchFaculties()
            .then(faculties => {
                commit('setFaculties', faculties)
            })
    },
    addFaculty({
        commit
    }, faculty) {
        facultyService.postFaculty(faculty)
            .then(() => {
                commit('addFaculty', faculty)
            })
    },
    deleteFaculty({
        commit
    }, id) {
        facultyService.deleteFaculty(id)
        commit('deleteFaculty', id)
    }
}

const mutations = {
    setFaculties(state, faculties) {
        state.faculties = faculties
    },
    addFaculty(state, faculty) {
        state.faculties.push(faculty)
    },
    deleteFaculty(state, id) {
        state.faculties = state.faculties.filter(obj => obj.pk !== id)
        console.log(state.faculties)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}