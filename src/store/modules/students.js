import studentService from '../../services/studentService'

const state = {
    students: []
}

const getters = {
    students: state => {
        return state.students
    }
}

const actions = {
    getStudents({
        commit
    }) {
        studentService.fetchStudents()
            .then(students => {
                commit('setStudents', students)
            })
    },
    addStudent({
        commit
    }, student) {
        studentService.postStudent(student)
            .then(() => {
                commit('addStudent', student)
            })
    },
    deleteStudent({
        commit
    }, id) {
        studentService.deleteStudent(id)
        commit('deleteStudent', id)
    }
}

const mutations = {
    setStudents(state, students) {
        state.students = students
    },
    addStudent(state, student) {
        state.students.push(student)
    },
    deleteStudent(state, id) {
        state.students = state.students.filter(obj => obj.pk !== id)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}