import attdService from "../../services/attdService";

const state = {
	attendences: [],
};

const getters = {
	attendences: (state) => {
		return state.attendences;
	},
};

const actions = {
	getAttendences({ commit }) {
		attdService.fetchAttendences().then((attendences) => {
			commit("setAttendences", attendences);
		});
	},
	addAttendence({ commit }, attendence) {
		attdService.postAttendence(attendence).then(() => {
			commit("addAttendence", attendence);
		});
	},
	deleteAttendence({ commit }, id) {
		attdService.deleteAttendence(id);
		commit("deleteAttendence", id);
	},
};

const mutations = {
	setAttendences(state, attendences) {
		state.attendences = attendences;
	},
	addAttendence(state, attendence) {
		state.attendences.push(attendence);
	},
	deleteAttendence(state, id) {
		state.attendences = state.attendences.filter((obj) => obj.pk !== id);
	},
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations,
};
