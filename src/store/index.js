import Vue from 'vue'
import Vuex from 'vuex'
import faculties from './modules/faculties'
import students from './modules/students'
import attendences from './modules/attendanceTaken'



Vue.use(Vuex)


export default new Vuex.Store({
  modules: {

    faculties,
    students,
    attendences
  }
})