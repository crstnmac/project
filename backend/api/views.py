from django.views.generic import TemplateView
from django.views.decorators.cache import never_cache
from rest_framework import viewsets
from rest_framework.decorators import action
from django.contrib.auth.models import User
from rest_framework import generics, permissions
from celery.result import AsyncResult
from rest_framework.response import Response


# from rest_framework import generics

from .models import Class, Subject,  Faculty, Student, Images, AttendanceTaken

from .serializers import SubjectSerializer, ClassSerializer, \
    FacultySerializer, StudentSerializer, ImageSerializer, \
    AttendanceTakenSerializer, UserSerializer

# Serve Vue Application
index_view = never_cache(TemplateView.as_view(template_name='index.html'))


class FacultyViewSet(viewsets.ModelViewSet):
    queryset = Faculty.objects.all()
    serializer_class = FacultySerializer


class ClassViewSet(viewsets.ModelViewSet):
    queryset = Class.objects.all()
    serializer_class = ClassSerializer


class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class SubjectViewSet(viewsets.ModelViewSet):
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer


class ImageViewSet(viewsets.ModelViewSet):
    queryset = Images.objects.all()
    serializer_class = ImageSerializer


class AttendanceTakenViewSet(viewsets.ModelViewSet):
    queryset = AttendanceTaken.objects.all()
    serializer_class = AttendanceTakenSerializer
    permission_classes = (permissions.IsAuthenticated,)

    @action(methods=['GET'], detail=True)
    def monitor_inference_progress(self, request, slug):
        inference_obj = self.get_object()
        progress = 100
        result = AsyncResult(inference_obj.task_id)
        if isinstance(result.info, dict):
            progress = result.info['progress']
        description = result.state
        return Response({'progress': progress, 'description': description}, status=status.HTTP_200_OK)


class UserList(generics.ListAPIView):
    """ View to list all users"""
    queryset = User.objects.all().order_by('first_name')
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated,)


class UserCreate(generics.CreateAPIView):
    """ View to create a new user. Only accepts POST requests """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAdminUser, )


class UserRetrieveUpdate(generics.RetrieveUpdateAPIView):
    """ Retrieve a user or update user information.
    Accepts GET and PUT requests and the record id must be
     provided in the request """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated, )
