from .tasks import recognize
from rest_framework import serializers
from .models import Faculty, Subject, Student, Class, Images, AttendanceTaken
from django.contrib.auth.models import User


class FacultySerializer(serializers.ModelSerializer):

    first_name = serializers.CharField(
        source='faculty_name.first_name')
    last_name = serializers.CharField(
        source='faculty_name.last_name')
    username = serializers.CharField(source='faculty_name.username')

    class Meta:
        model = Faculty
        fields = ('first_name', 'last_name', 'username', 'department')


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = ('url', 'subject', 'subject_name', 'faculty_name', 'pk')


class ClassSerializer(serializers.ModelSerializer):
    class Meta:
        model = Class
        fields = ('url', 'branch', 'section', 'year',
                  'no_of_students', 'pk')


class StudentSerializer(serializers.ModelSerializer):

    sections = serializers.CharField(source='section.branch')
    clsection = serializers.CharField(source='section.section')
    yrsection = serializers.CharField(source='section.year')

    class Meta:
        model = Student
        fields = ('url', 'usn', 'name', 'section',
                  'sections', 'clsection', 'yrsection', 'pk')


class ImageSerializer(serializers.ModelSerializer):

    student_usn = serializers.CharField(source='student.usn')
    student_name = serializers.CharField(source='student.name')
    image = serializers.ListField(
        child=serializers.FileField(max_length=100000,
                                    allow_empty_file=False,
                                    use_url=False)
    )

    class Meta:
        model = Images
        fields = ('student', 'student_usn', 'name', 'image')


class AttendanceTakenSerializer(serializers.ModelSerializer):

    class Meta:
        model = AttendanceTaken
        fields = ['subject_code']

    def create(self, validated_data):
        subject_code = validated_data.get('subject_code')
        # or simply Inference.objects.create(**validated_data)
        inference_obj = AttendanceTaken.objects.create(
            subject_code=subject_code,)
        recognize.delay(inference_obj.subject_code)  # run inference async
        return inference_obj

    # def get_subject(self, instance):
    #     subject_name = instance.subject_code.name

    #     with open('subject.txt', 'w') as f:
    #         f.write(subject_name + '\n')


class UserSerializer(serializers.ModelSerializer):
    """ A serializer class for the User model """
    class Meta:
        # Specify the model we are using
        model = User
        # Specify the fields that should be made accessible.
        # Mostly it is all fields in that model
        fields = ('id', 'first_name', 'last_name', 'username',
                  'password', 'is_active', 'is_superuser')
