from django.contrib import admin
from .models import Class, Student, Subject, Faculty, Images, ImageFile, \
    Attendance, AttendanceTaken
# Register your models here.
import csv
from django.http import HttpResponse
from .tasks import train

admin.AdminSite.site_header = "Autosense Admin Panel"
admin.AdminSite.site_title = "Autosense"


class ExportCsvMixin:
    def export_as_csv(self, request, queryset):

        meta = self.model._meta
        field_names = [field.name for field in meta.fields]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(
            meta)
        writer = csv.writer(response)

        writer.writerow(field_names)
        for obj in queryset:
            row = writer.writerow([getattr(obj, field)
                                   for field in field_names])

        return response

    export_as_csv.short_description = "Export Selected"


class TrainMixin:

    def trainMixin(self, request, queryset):
        train()

    trainMixin.short_description = 'Train Model'


class ClassAdmin(admin.ModelAdmin):
    list_display = ('branch', 'year', 'section')
    list_filter = ('branch', 'year')


admin.site.register(Class, ClassAdmin)


class StudentAdmin(admin.ModelAdmin, ExportCsvMixin):
    list_display = ('usn', 'name', 'section')
    list_filter = ('section__branch', 'section__year', 'section__section')
    actions = ["export_as_csv"]


admin.site.register(Student, StudentAdmin)


class FacultyAdmin(admin.ModelAdmin):
    list_display = ('faculty_name', 'department')


admin.site.register(Faculty, FacultyAdmin)


class SubjectAdmin(admin.ModelAdmin):
    list_display = ('subject_id', 'subject_name', 'faculty_name')


admin.site.register(Subject, SubjectAdmin)


class ImageFileAdmin(admin.StackedInline):
    model = ImageFile


@admin.register(Images,)
class ImageAdmin(admin.ModelAdmin, TrainMixin):

    inlines = [ImageFileAdmin]
    actions = ['trainMixin']


@admin.register(ImageFile, )
class ImageFileAdmin(admin.ModelAdmin):
    pass


class AttendanceTakenAdmin(admin.ModelAdmin):
    list_display = ['subject_code']


admin.site.register(AttendanceTaken, AttendanceTakenAdmin)


class AttendenceView(admin.ModelAdmin, ExportCsvMixin):
    list_display = ('usn', 'attendance',  'subject_code', 'date', 'hour',)
    list_filter = ('subject_code', 'hour', 'attendance')
    date_hierarchy = 'date'
    actions = ["export_as_csv"]


admin.site.register(Attendance, AttendenceView)
