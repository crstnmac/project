from backend.celery import app
from backend.settings.dev import MEDIA_ROOT as path
import cv2
import os
import numpy as np
from datetime import datetime
import os.path
import sqlite3


@app.task(bind=True, name="Recognize")
def recognize(self, subject_code):

    # load model architecture

    def read_students(data_folder_path):
        students = os.listdir(data_folder_path)
        student_list = ["None"]
        for dir_name in students:
            if not dir_name.startswith("4VP"):
                continue
            student_list.append(dir_name)
        return student_list

    subjects = read_students(path + "images")
    face_recognizer = cv2.face.LBPHFaceRecognizer_create()
    face_recognizer.read(path + 'trainer/trainer.yml')
    present_students = []

    def resize_image(img):
        dim = (240, 240)
        return cv2.resize(img, dim, interpolation=cv2.INTER_AREA)

    def detect_faces(img):
        detected_faces = []
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        face_cascade = cv2.CascadeClassifier(
            path+'opencv-files/lbpcascade_frontalface.xml')
        faces = face_cascade.detectMultiScale(
            gray, scaleFactor=1.2, minNeighbors=5)
        if len(faces) == 0:
            return None, None
        for (x, y, w, h) in faces:
            required_face = resize_image(gray[y:y + w, x:x + h])
            detected_faces.append(required_face)
        return tuple(detected_faces), tuple(faces)

    def draw_rectangle(img, rect):
        (x, y, w, h) = rect
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 15)

    def draw_text(img, text, x, y):
        cv2.putText(img, text, (x, y), cv2.FONT_HERSHEY_PLAIN,
                    10, (0, 255, 0), 15)

    def predict(test_img):
        img = test_img.copy()
        faces, rects = detect_faces(img)
        try:
            for individual_face_details in zip(faces, rects):
                label, confidence = face_recognizer.predict(
                    individual_face_details[0])
                print(subjects[label], " - ", confidence)
                if confidence > 70:
                    label_text = 'unknown'
                    draw_rectangle(img, individual_face_details[1])
                    (x, y, w, h) = individual_face_details[1]
                    draw_text(img, label_text, x, h)
                else:
                    label_text = subjects[label]
                    present_students.append(label_text)
                    draw_rectangle(img, individual_face_details[1])
                    (x, y, w, h) = individual_face_details[1]
                    draw_text(img, label_text, x, h)
        except:
            print("")

    #    return img

    def check_attendance():
        file_name = path + 'temp_attendance_file/current_attendance.txt'
        if not os.path.exists(file_name):
            attendance_file = open(file_name, "w")
            attendance_file.close()

        attendance_file_handler = open(file_name, 'r')
        past_result = attendance_file_handler.readline()
        past_result_list = list(past_result.split(" "))
        attendance_file_handler.close()
        for student in present_students:
            if student not in past_result_list:
                past_result_list.append(student)
        current_students = listToString(past_result_list)
        print(current_students)
        attendance_file_handler = open(file_name, "w")
        attendance_file_handler.write(current_students)
        attendance_file_handler.close()

    def listToString(s):
        s = [x.strip() for x in s]
        str1 = " ".join(s)
        return str1

    print('predicting images')

    data_dir = path + "test-data"
    data = os.listdir(data_dir)
    for im in data:
        print(im)
        test_img = cv2.imread(data_dir+"/"+im)
        predict(test_img)
        os.remove(data_dir+"/"+im)

    check_attendance()

    def get_current_hour():
        now = datetime.now()
        date = now.strftime("%Y-%m-%d")
        hour = now.strftime("%H")
        h = 0
        if hour == '9':
            h = 1
        elif hour == '10':
            h = 2
        elif hour == '11':
            h = 3
        elif hour == '12':
            h = 4
        elif hour == '14':
            h = 5
        elif hour == '15':
            h = 6
        elif hour == '16':
            h = 7
        else:
            h = 0
        return date, h

    def get_present_students(file_path):
        # if not os.path.exists(file_path):
        #     sys.exit()
        file_handler = open(file_path)
        present_students_string = file_handler.readline()
        present_students_list = list(present_students_string.split(" "))
        present_students_list = [x.strip() for x in present_students_list]
        file_handler.close()
        return present_students_list

    def get_subject_name(file_path):
        # if not os.path.exists(file_path):
        #     sys.exit()
        file_handler = open(file_path)
        subject_name = file_handler.readline()
        subject_name = subject_name.strip()
        return subject_name

    def update_db(date, hour, subject, student_list):
        conn = sqlite3.connect('db.sqlite3')
        cursor = conn.execute("select * from api_student")
        for row in cursor:
            print(row[1])
            if row[1] in student_list:
                data_tuple = (1, date, hour, subject, row[1])
                conn.execute(
                    '''insert into api_attendance(attendance,date,hour,subject_code,usn) values(?,?,?,?,?)''', data_tuple)
                conn.commit()
            else:
                data_tuple = (0, date, hour, subject, row[1])
                conn.execute(
                    '''insert into api_attendance(attendance,date,hour,subject_code,usn) values(?,?,?,?,?)''', data_tuple)
                conn.commit()
    file_path = path + "temp_attendance_file/current_attendance.txt"
    student_list = get_present_students(file_path)
    print(student_list)

    with open(path + 'current_subject/current_subject.txt', 'w') as f:
        f.write(str(subject_code))

    file_path = path + 'current_subject/current_subject.txt'
    subject_name = get_subject_name(file_path)

    date, hour = get_current_hour()

    update_db(date, hour, subject_name, student_list)
    os.remove(path+"temp_attendance_file/current_attendance.txt")
    os.remove(path + "current_subject/current_subject.txt")

    print('Attendance recorded')


images = path+"images"


@app.task(bind=True, name="Train_Model")
def train(self):
    def read_students(data_folder_path):
        students = os.listdir(data_folder_path)
        student_list = ["None"]
        for dir_name in students:
            if not dir_name.startswith("4VP"):
                continue
            student_list.append(dir_name)
        return student_list

    subjects = read_students(images)

    def resize_image(img):
        dim = (240, 240)
        return cv2.resize(img, dim, interpolation=cv2.INTER_AREA)

    def detect_face(img):
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        face_cascade = cv2.CascadeClassifier(
            path + 'opencv-files/lbpcascade_frontalface.xml')

        faces = face_cascade.detectMultiScale(
            gray, scaleFactor=1.2, minNeighbors=5)

        if (len(faces) == 0):
            return None, None

        (x, y, w, h) = faces[0]
        face = resize_image(gray[y:y+w, x:x+h])

        return face, faces[0]

    def prepare_training_data(data_folder_path):

        dirs = os.listdir(data_folder_path)

        faces = []
        labels = []
        i = 1
        for dir_name in dirs:

            if not dir_name.startswith("4VP"):
                continue

            label = i
            i += 1

            subject_dir_path = data_folder_path + "/" + dir_name

            subject_images_names = os.listdir(subject_dir_path)

            for image_name in subject_images_names:

                if image_name.startswith("."):
                    continue

                image_path = subject_dir_path + "/" + image_name

                image = cv2.imread(image_path)

                # cv2.imshow("Training on image...",
                #            cv2.resize(image, (400, 500)))
                # cv2.waitKey(100)

                face, rect = detect_face(image)

                if face is not None:
                    faces.append(face)
                    labels.append(label)

        # cv2.destroyAllWindows()
        # cv2.waitKey(1)
        # cv2.destroyAllWindows()

        return faces, labels

    print("Preparing data...")
    faces, labels = prepare_training_data(images)
    print("Data prepared")

    print("Total faces: ", len(faces))
    print("Total labels: ", len(labels))
    print(labels)

    face_recognizer = cv2.face.LBPHFaceRecognizer_create()

    face_recognizer.train(faces, np.array(labels))

    face_recognizer.write(path + 'trainer/trainer.yml')
