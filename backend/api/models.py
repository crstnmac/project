from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class Faculty(models.Model):
    faculty_name = models.ForeignKey(
        User, limit_choices_to={'groups__name': 'Lectures'}, on_delete=models.CASCADE)
    department = models.CharField(max_length=20, default="")

    def __str__(self):
        return "{}-{}".format(self.faculty_name, self.department)


class Subject(models.Model):
    subject_id = models.CharField(max_length=10, default="")
    subject_name = models.CharField(max_length=32, default="")
    faculty_name = models.ForeignKey(Faculty, on_delete=models.CASCADE)

    def __str__(self):
        return "{}-{}".format(self.faculty_name, self.subject_id)


class Class(models.Model):
    branch = models.CharField(max_length=5, default="")
    section = models.CharField(max_length=1, default="")
    year = models.IntegerField(default=1)
    no_of_students = models.IntegerField(default=60)

    def __unicode__(self):
        return self.branch

    def __str__(self):
        return "{}{}{}".format(self.year, self.branch, self.section)


class Student(models.Model):
    usn = models.CharField(max_length=10, default="")
    name = models.CharField(max_length=32, default="")
    section = models.ForeignKey(Class, on_delete=models.CASCADE)

    def __str__(self):
        return "{}{}".format(self.usn, self.name)


def get_user_image_folder(instance, filename):
    return 'images/{name}/{filename}'.format(
        name=instance.student.student.usn, filename=filename)


class Images(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)

    def __str__(self):
        return "{}".format(self.student)


class ImageFile(models.Model):
    student = models.ForeignKey(Images, on_delete=models.CASCADE)
    image = models.ImageField(
        upload_to=get_user_image_folder, verbose_name='Image', null=False)

    def __str__(self):
        return "{}".format(self.student)


class Attendance(models.Model):
    date = models.DateField(default=timezone.now)
    hour = models.IntegerField(default=0, unique=False)
    subject_code = models.CharField(max_length=10, default="", unique=False)
    usn = models.CharField(max_length=10, default="", unique=False)
    attendance = models.IntegerField(default=0, unique=False)

    def __str__(self):
        return "{}{}{}{}".format(self.date, self.hour, self.usn, self.subject_code)


class AttendanceTaken(models.Model):
    subject_code = models.CharField(max_length=10, default="")

    def __str__(self):
        return "{}".format(self.subject_code)
