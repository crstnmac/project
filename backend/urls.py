"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
"""

from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from django.conf.urls import url

from rest_framework_simplejwt.views import (
    TokenObtainPairView, TokenRefreshView)
from .api.views import index_view,  FacultyViewSet, \
    ClassViewSet, SubjectViewSet, StudentViewSet, ImageViewSet, AttendanceTakenViewSet


router = routers.DefaultRouter()
router.register(r'students',  StudentViewSet)
router.register(r'subject', SubjectViewSet)
router.register(r'faculty', FacultyViewSet)
router.register(r'class', ClassViewSet)
router.register(r'images', ImageViewSet)
router.register(r'takeattd', AttendanceTakenViewSet)


urlpatterns = [
    # http://localhost:8000/
    path('', index_view, name='index'),

    # http://localhost:8000/api/<router-viewsets>
    path('api/', include(router.urls)),

    # http://localhost:8000/api/admin/
    path('api/admin/', admin.site.urls),
    url(r'api/login/', include(
        'rest_framework.urls', namespace='rest_framework')),
    url(r'api/login/token/obtain/', TokenObtainPairView.as_view()),
    url(r'api/login/token/refresh/', TokenRefreshView.as_view()),
]
